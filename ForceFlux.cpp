#pragma once

#include "Common.h" 
#include "TestCylinder.h"

#ifdef VISUAL
#include "ErrorCheck.h"
#include<GL/glew.h>
#include<glfw3.h>
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include "Shader.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"
#endif // VISUAL

using namespace std;
int ScreenWidth = 2400;
int ScreenHeight = 1350;

keyAcion m_KeyAction = keyAcion::non;
float m_xPos = ScreenWidth / 2;
float m_yPos = ScreenHeight / 2;
float m_Fov = 45.0f;
bool m_LeftButtonPressed = false;
bool m_RightButtonPressed = false;


#ifdef VISUAL

void processInput(GLFWwindow* window, TestCylinder* currentTest, float scroll)
{
	bool reset = false;

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::leftArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::rightArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::upArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::downArrow;
	}
	
	if(m_KeyAction != keyAcion::non && m_KeyAction != keyAcion::mouse)
		currentTest->OnUpdateCameraKey(m_KeyAction);

	if (m_KeyAction == keyAcion::mouse && m_LeftButtonPressed)
	{
		currentTest->OnUpdateCameraMouseRotate(m_xPos, m_yPos);
		reset = true;
	}
	
	if(reset)
		m_KeyAction = keyAcion::non;

	if (scroll != 0)
	{	
		m_Fov -= scroll;
		currentTest->OnUpdateCameraScroll(m_Fov);
	}
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	m_KeyAction = keyAcion::mouse;
	m_xPos = xpos;
	m_yPos = ypos;
}

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

void ButtonFeedback(GLFWwindow* window)
{
	int stateLeft = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (stateLeft == GLFW_PRESS)
		m_LeftButtonPressed = true;
	else if (stateLeft == GLFW_RELEASE)
		m_LeftButtonPressed = false;

	int stateRight = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
	if (stateRight == GLFW_PRESS)
		m_RightButtonPressed = true;
	else if (stateRight == GLFW_RELEASE)
		m_RightButtonPressed = false;
}

#endif // VISUAL


SolverSettings SolverSetup(simulationType simType)
{
	SolverSettings ss;

	if (simType == simulationType::Cylinder || simType == simulationType::SplitCylinder)
	{
		ss.dt = 0.0000015f; //0.000002f; 
		ss.co = 0.98f; //0.98f;
		ss.t = 0.0f;
		ss.t2 = ss.t + ss.dt;
		ss.t1 = 0.5 * (ss.t + ss.t2);
	}
	else if (simType == simulationType::ModulusOfRupture)
	{
		ss.dt = 0.0000018f; //0.000002f; 
		ss.co = 0.98f; //0.98f;
		ss.t = 0.0f;
		ss.t2 = ss.t + ss.dt;
		ss.t1 = 0.5 * (ss.t + ss.t2);
	}
	else if (simType == simulationType::DirectTension)
	{
		ss.dt = 0.0000018f; //0.000002f; 
		ss.co = 0.98f; //0.98f;
		ss.t = 0.0f;
		ss.t2 = ss.t + ss.dt;
		ss.t1 = 0.5 * (ss.t + ss.t2);
	}
	else if (simType == simulationType::SteelTest)
	{
		ss.dt = 0.00000007f;  // 0.00000007f; //0.000002f; 
		ss.co = 0.98f; //0.98f;
		ss.t = 0.0f;
		ss.t2 = ss.t + ss.dt;
		ss.t1 = 0.5 * (ss.t + ss.t2);
	}

	return ss;
}

MaterialSettings SetupMaterial(simulationType simType, float elongationLimit, float yieldStrain)
{
	//Data from: https://www.engineeringtoolbox.com/concrete-properties-d_1223.html
	//Compressive strength : 20 - 40 MPa(3000 - 6000 psi)
	//Flexural strength : 3 - 5 MPa(400 - 700 psi)
	//Tensile strength - ? : 2 - 5 MPa(300 - 700 psi)

	MaterialSettings ms;

	if (simType != simulationType::SteelTest)  //Concrete
	{
		ms.rho = 2400.0f;		//2240 - 2400 Kg/m3
		ms.E = 40e9f;			//14 - 41 Gpa
		ms.v = 0.2f;			//0.2 - 0.21
		ms.G = ms.E / (2.0f * (1.0f + ms.v));				//21Gpa
		ms.K = ms.E / (3.0f * (1.0f - 2.0f * ms.v));
		ms.yieldStrain = yieldStrain;  //1.0f / 800.0f;		// 1/800
		ms.elongationLimit = elongationLimit;   //1.0f / 100000.0f; //100000.0f;
	}
	else  //Steel
	{
		ms.rho = 7800.0f;
		ms.E = 210e9f;
		ms.v = 0.28f;
		ms.G = ms.E / (2.0f * (1.0f + ms.v));
		ms.K = ms.E / (3.0f * (1.0f - 2.0f * ms.v));
		ms.yieldStrain = yieldStrain; //1.0f / 1500.0f;		// 1/800
		ms.elongationLimit = elongationLimit;  //1.0f / 6000;		//10000.0f; //100000.0f;
	}

	return ms;
}

KernelSettings SetupKernel(float alpha) 
{
	KernelSettings ks;

	int k = 2;
	float pi = 3.1415f;
	float C = (float)Factorial(2 * k + 3) / (4.0f * pi * pow(alpha, 3) * pow(2.0f, (2.0f * k)) * pow((float)Factorial(k), 2.0f));
	float C2 = (8.0f / 105.0f) * pow(alpha, 3);
	C = 2.0f;

	ks.alpha = alpha;
	ks.alphaSquared = alpha * alpha;
	ks.alphaPowThree = pow(alpha, 3);
	ks.alphaPowSeven = pow(alpha, 7);
	ks.k = k;
	ks.C = C;

	ks.rndSeed = 1;

	return ks;
}

DisplacementSettings SetupDisp(simulationType simType)
{
	DisplacementSettings ds;

	if (simType == simulationType::Cylinder)
	{
		ds.magnitude = 0.004;
		ds.nDispSteps = 4000;
		ds.stepSize = 1.0000e-06; // 0.004 / 4000;
		ds.stepCounter = 1;
	}
	else if (simType == simulationType::SplitCylinder)
	{
		ds.magnitude = 0.003;
		ds.nDispSteps = 2000;
		ds.stepSize = 1.5000e-06; // 0.002 / 2000;
		ds.stepCounter = 1;
	}
	else if (simType == simulationType::ModulusOfRupture)
	{
		ds.magnitude = 0.002;
		ds.nDispSteps = 5000;
		ds.stepSize = 4.0000e-07; // 0.002 / 5000;
		ds.stepCounter = 1;
	}
	else if (simType == simulationType::DirectTension)
	{
		ds.magnitude = 0.001;
		ds.nDispSteps = 5500;
		ds.stepSize = 1.8182e-07;   //0.001/ 5500;
		ds.stepCounter = 1;
	}
	else if (simType == simulationType::SteelTest)
	{
		ds.magnitude = 0.0025;  //0.003
		ds.nDispSteps = 100000;
		ds.stepSize = 2.5e-8; // ds.magnitude / ds.nDispSteps;
		ds.stepCounter = 1;
	}

	return ds;
}

void RunBlind(simulationType simType, SolverSettings solver, MaterialSettings material, KernelSettings kernel, DisplacementSettings disp, float strainLimit, string fileNameSuffix)
{
	//Initialise the model
	TestCylinder* currentTest = new TestCylinder(simType, solver, material, kernel, disp, strainLimit, fileNameSuffix);

	std::cout << "\n" << "Non-GUI Simulation initiated" << "\n" << std::endl;
	bool run = true;

	int counter = 0;

	while (run)
	{
		currentTest->OnUpdateAnalysis(); 
		currentTest->OnUpdateTime();

		float strain = currentTest->GetStrain();

		if (strain > strainLimit)
		{
			currentTest->Output();
			run = false;
		}

		//Save results every 10th iteration
		if (counter % 10 == 0) 
		{
			currentTest->SaveResults();
		}

		//Write the results to file every 100th iteration
		if (counter % 100 == 0)
		{
			currentTest->Output();
		}

		counter++;
	}
}

void RunVisual(simulationType simType, SolverSettings solver, MaterialSettings material, KernelSettings kernel, DisplacementSettings disp, float strainLimit, string fileNameSuffix)
{

#ifdef VISUAL

	/*いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい*/
	//Initialise OpenGL following the The Cherno - OpenGL Series on youtube
	/*いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい*/


	m_LeftButtonPressed = true;
	m_RightButtonPressed = false;

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);

	/* Initialize the library */
	if (!glfwInit())
		return; //-1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(ScreenWidth, ScreenHeight, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return;// -1;
	}

	const char* glfwVersion = glfwGetVersionString();
	glfwSetCursorPosCallback(window, mouse_callback);

	/* Make the window's context current */
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1); //

	//Needs to be initialised after we have a valid context
	if (glewInit() != GLEW_OK)
		std::cout << "Error at GLEW initialisation!" << std::endl;

	std::cout << glGetString(GL_VERSION) << std::endl;

	//Initialise the model
	TestCylinder* currentTest = new TestCylinder(simType, solver, material, kernel, disp, strainLimit, fileNameSuffix);
	currentTest->InitialiseOpenGL();

	//Scope definition for variable life etc.
	{
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		//Initialisation of ImGUI
		ImGui::CreateContext();
		ImGui_ImplGlfwGL3_Init(window, true);
		ImGui::StyleColorsDark();

		/* Loop until the user closes the window */
		while (!glfwWindowShouldClose(window))
		{
			GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
			GLCall(glClear(GL_COLOR_BUFFER_BIT));

			ImGui_ImplGlfwGL3_NewFrame();
			if (currentTest) //Testing that the object is not null
			{
				ImVec2 wPos = ImGui::GetWindowPos();
				processInput(window, currentTest, ImGui::GetIO().MouseWheel);

				currentTest->OnUpdateAnalysis();
				currentTest->OnUpdateTime();
				currentTest->OnUpdateOpenGL();
				currentTest->OnRender();

				ImGui::Begin("Test");
				currentTest->OnImGuiRenderer();		//Registering a new test based on the button clicked.
				ImGui::End();
			}

			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
			GLCall(glfwSwapBuffers(window));
			GLCall(glfwPollEvents());

			ButtonFeedback(window);
		}

		delete currentTest;

	}//Scope to avoid some OpenGL error (Video Abstracting OpenGL into Classes)

	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();
	glfwTerminate();

	return;

#endif //VISUAL
}

bool IsValidInput(std::vector<float> input) 
{
	for (int i = 0; i < input.size(); i++)
	{
		if (input[i] < 0.0f)
			return false;

	}

	return true;
}


int main()
{	
	float alpha = 4.5;

	//Parameters to be replace by command line input
	int runIndex = 1;						
	float yieldStain = 0.00066;			//1 / 1500 ~ 0.00066
	float elongationLimit = 0.00015;	// 1.0f / 6000 ~ 0.00016 - > 13 % of average arm length. 0.0012 -> 100 % of average arm length
	float strainLimit = 0.025;

	//Commandline input example. Copy below:
	// 1 0.00066 0.00015 0.025 

	string fileNameSuffix = "";

	std::cout<< "Enter 4 values (runIndex, yieldStrain, elongationLimit, strainLimit)" <<std::endl;
	
	float a, b, c, d;
	std::cin >> a >> b >> c >> d;
	std::vector<float> input;
	input.insert(input.end(), { a, b, c, d});

	std::cout << "Input length: " << input.size() << std::endl;

	//Overriding predefined values based on keyboard input
	if (input.size() == 4 && IsValidInput(input)) 
	{
		string suffixAddition = "";

		runIndex = (int)input[0];

		if(runIndex < 10)
			suffixAddition += "_Run_0" + std::to_string(runIndex);
		else
			suffixAddition += "_Run_" + std::to_string(runIndex);

				
		if (yieldStain != input[1])
		{
			yieldStain = input[1];
			suffixAddition += "_yield_" + std::to_string(yieldStain);
		}
		
		if (elongationLimit != input[2])
		{
			elongationLimit = input[2];
			suffixAddition += "_elong_" + std::to_string(elongationLimit);
		}
		
		if (strainLimit != input[3])
		{
			strainLimit = input[3];
			suffixAddition += "_limit_" + std::to_string(strainLimit);
		}

		fileNameSuffix += suffixAddition;

		std::cout << "Successful parameter entry" << std::endl;
		std::cout << "Suffix: " << fileNameSuffix << std::endl;
	}
	else
	{
		std::cout << "Insuficient parameters input, program aborted" << std::endl;
		return 1;
	}


	simulationType simType = simulationType::SteelTest;
	SolverSettings solver = SolverSetup(simType);
	MaterialSettings material = SetupMaterial(simType, elongationLimit, yieldStain);
	KernelSettings kernel = SetupKernel(alpha);
	DisplacementSettings disp = SetupDisp(simType);


	
#ifdef VISUAL 
	{
		RunVisual(simType, solver, material, kernel, disp, strainLimit, fileNameSuffix);
	}
#else
	{
		RunBlind(simType, solver, material, kernel, disp, strainLimit, fileNameSuffix);
	}
#endif


	return 0;
}







