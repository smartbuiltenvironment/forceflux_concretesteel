These instructions will show how to setup the project for developement with Visual Studio. I have used Visual Studio 2019 for the development with Visual C++ 2015.   


Depedencies:

Create a new directory at the root location for the code (same folder as .sln file), called "source", and create an other folder inside called "vendor". "source/vendor" will be the installation location for two packages, GLM and IMGUI. 

To install GLM:

    1. Go to https://github.com/g-truc/glm/releases/tag/0.9.9.8
    2. At the releases page download the latest version of GLM
    3. Unzip glm.zip and copy the content from the glm folder to: "source/vendor"

GLM is a header only library. No linking is needed in Visual studio.

To install imgui:

    1. Go to https://github.com/ocornut/imgui/releases
    2. Download a version (I have been using 1.6)
    3. Copy the files in the folder to "source/vendor/imgui"
    4. For help see: https://www.youtube.com/watch?v=nVaQuNXueFw


For the installation of OpenGL, create a folder called "Depend" at the root of the code repository (same folder as .sln file). In this folder create subfolders so that we get "Depend/GLFW" and "Depend/GLEW". We will then copy the useful library files into these folders following the instructions below:

To install OpenGL we need GLFW which is installed as follows:
    
    1. Go to https://www.glfw.org/ and click Download in the menu at the top.
    2. At the downloads page, select appropriate version (I use x64 for windows, GLFW version 3.3.3)
    3. Unzip the file and copy the folder called "include" and "lib-vc2015" (the version matching your installation of Visual C++) to "Depend/GLFW"
    4. Setup linking following the steps below.    
    5. Tutorial for instructions: https://www.youtube.com/watch?v=OR4fNpBjmq8&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=2
    

To install OpenGL we also need GLEW, which is installed following:
    
    1. Go to https://glew.sourceforge.net/
    2. Click on "Binaries for Windows" to donwload GLEW 2.1.0
    3. Unzip the downloaded file.
    4. Copy all the content in folder "glew-2.1.0" to "Depend/GLEW"
    5. Setup linking following the steps below.  
    6. Tutorial for instructions: https://www.youtube.com/watch?v=H2E3yO0J7TM&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=3


To specify that we want to link to the GLEW library statically we need to specify that in the settings of Visual Studio. 

    1. Right click on the project "ForceFlux" and click on properties
    2. Go to: "Configuration Properties => C/C++ => Preprocessor => Preprocessor Definitions" 
    3. Click the down arrow and select "edit"
    4. Add the keyword "GLEW_STATIC"


Adding dependencies to Visual Studio:

    1. Right click on the project "ForceFlux" and click on properties
    2. Go to: "Configuration Properties => C/C++ => General => Additional Include Directories" 
    3. Click the down arrow and select "edit"
    4. In the new panel that pops up, add the following lines:

        $(SolutionDir)source\vendor
        $(SolutionDir)Depend\GL\inc
        $(SolutionDir)Depend\GLEW\include
        $(SolutionDir)Depend\GLWF\inc


Adding libraries:

    1. Right click on the project "ForceFlux" and click on properties
    2. Go to: "Configuration Properties => Linker => General => Additional Library Directories" 
    3. Click the down arrow and select "edit"
    4. In the new window that pops up, add the following lines:

        $(SolutionDir)Depend \GL\lib
        $(SolutionDir)Depend\GLEW\lib\Release\x64
        $(SolutionDir)Depend\GLWF\lib


Adding links:

    1. Right click on the project "ForceFlux" and click on properties
    2. Go to: "Configuration Properties => Linker => Input => Additional Depedencies" 
    3. Click the down arrow and select "edit"
    4. In the new window that pops up, add the following lines:

        glfw3.lib
        opengl32.lib
        glew32s.lib
        %(AdditionalDependencies)


(For reference I have the following links that show below in the same window:

Inherited values:

kernel32.lib
user32.lib
gdi32.lib
winspool.lib
comdlg32.lib
advapi32.lib
shell32.lib
ole32.lib
oleaut32.lib
uuid.lib
odbc32.lib
odbccp32.lib
cudart.lib
cudadevrt.lib)





